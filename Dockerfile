FROM ubuntu:focal
LABEL maintainer="David Plugge <dev@davidplugge.de>"
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && apt-get install -qy \
  ansible \
  make git build-essential \
  && rm -rf /var/lib/apt/lists/*
RUN ansible-galaxy collection install community.general

WORKDIR /data
VOLUME ["/data"]